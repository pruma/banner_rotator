#!/usr/bin/env python
from setuptools import setup, find_packages

setup(name='django-baner-rotator',
    version='0.1',
    description='',
    author='Pupkov Semen',
    author_email='semen.pupvko@gmail.com',
    url='https://bitbucket.org/pruma/banner_rotator',
    include_package_data=True,
    zip_safe=False,
    packages=find_packages(),
    classifiers=[
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ]
)
